Source: golang-github-ngaut-go-zookeeper
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 10),
               dh-golang,
               golang-any,
               golang-github-samuel-go-zookeeper-dev
Standards-Version: 4.1.1
Homepage: https://github.com/ngaut/go-zookeeper
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-ngaut-go-zookeeper
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-ngaut-go-zookeeper.git
Testsuite: autopkgtest-pkg-go
XS-Go-Import-Path: github.com/ngaut/go-zookeeper

Package: golang-github-ngaut-go-zookeeper-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-samuel-go-zookeeper-dev
Description: Native ZooKeeper client for Go
 This package provides a native ZooKeeper (client) library for
 communicating with ZooKeeper servers.
 .
 Example:
     func main() {
         c, _, err := zk.Connect([]string{"127.0.0.1"}, time.Second) //*10)
         if err != nil {
             panic(err)
         }
         children, stat, ch, err := c.ChildrenW("/")
         if err != nil {
             panic(err)
         }
         fmt.Printf("%+v %+v\n", children, stat)
         e := <-ch
         fmt.Printf("%+v\n", e)
     }
 .
 See also: https://godoc.org/github.com/samuel/go-zookeeper/zk
